FROM debian:bullseye
MAINTAINER Neil Armstrong <narmstrong@kernel.org>
LABEL Description="This image is for building u-boot inside a container"

# Make sure apt is happy
ENV DEBIAN_FRONTEND=noninteractive

# Update packages
RUN apt-get update -qq
RUN apt-get install -y wget xz-utils

# Download and decompress toolchain
RUN mkdir -p /opt/toolchain
RUN wget -O - https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/13.2.0/x86_64-gcc-13.2.0-nolibc-aarch64-linux.tar.xz | tar -C /opt/ -xJ

# Update and install things from apt now
RUN apt-get update && apt-get install -y \
	automake \
	autopoint \
	bc \
	binutils-dev \
	bison \
	build-essential \
	cgpt \
	coreutils \
	cpio \
	curl \
	device-tree-compiler \
	dosfstools \
	e2fsprogs \
	efitools \
	erofs-utils \
	expect \
	fakeroot \
	flex \
	gawk \
	gdisk \
	git \
	gnu-efi \
	gnutls-dev \
	graphviz \
	help2man \
	imagemagick \
	iputils-ping \
	libc6-i386 \
	libconfuse-dev \
	libftdi1-dev \
	libgit2-dev \
	libgpiod-dev \
	libjson-glib-dev \
	libguestfs-tools \
	libgnutls28-dev \
	libgnutls30 \
	liblz4-tool \
	libpixman-1-dev \
	libpython3-dev \
	libsdl1.2-dev \
	libsdl2-dev \
	libseccomp-dev \
	libslirp-dev \
	libssl-dev \
	libtool \
	libudev-dev \
	libusb-1.0-0-dev \
	libyaml-dev \
	lzma-alone \
	lzop \
	meson \
	mount \
	mtd-utils \
	mtools \
	net-tools \
	ninja-build \
	openssl \
	openssh-client \
	picocom \
	parted \
	pkg-config \
	python-is-python3 \
	python3 \
	python3-dev \
	python3-pip \
	python3-pyelftools \
	python3-sphinx \
	python3-virtualenv \
	rpm2cpio \
	sbsigntool \
	socat \
	softhsm2 \
	sparse \
	srecord \
	sudo \
	swig \
	texinfo \
	util-linux \
	uuid-dev \
	virtualenv \
	vboot-kernel-utils \
	vboot-utils \
	xxd \
	zip

# Create our user/group
RUN echo uboot ALL=NOPASSWD: ALL > /etc/sudoers.d/uboot
RUN useradd -m -U uboot
USER uboot:uboot

# Populate the cache for pip to use. Get these via wget as the
# COPY / ADD directives don't work as we need them to.
RUN wget -O /tmp/pytest-requirements.txt https://source.denx.de/u-boot/u-boot/-/raw/master/test/py/requirements.txt
RUN wget -O /tmp/buildman-requirements.txt https://source.denx.de/u-boot/u-boot/-/raw/master/tools/buildman/requirements.txt
RUN virtualenv -p /usr/bin/python3 /tmp/venv && \
	. /tmp/venv/bin/activate && \
	pip install -r /tmp/pytest-requirements.txt -r /tmp/buildman-requirements.txt && \
	deactivate && \
	rm -rf /tmp/venv /tmp/pytest-requirements.txt /tmp/buildman-requirements.txt

# Create the buildman config file
RUN /bin/echo -e "[toolchain]\nroot = /usr" > ~/.buildman
RUN /bin/echo -e "kernelorg = /opt/gcc-13.2.0-nolibc/*" >> ~/.buildman
RUN /bin/echo -e "\nsandbox = x86_64" >> ~/.buildman
